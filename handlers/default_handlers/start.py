from loader import bot
from keyboards.commands import create_main_keyboard


@bot.message_handler(commands=["start"])
def bot_start(message):
    # bot.reply_to(message, f"Привет, {message.from_user.full_name}!")
    bot.send_message(message.chat.id, f"Привет, {message.from_user.full_name}!", reply_markup=create_main_keyboard())
