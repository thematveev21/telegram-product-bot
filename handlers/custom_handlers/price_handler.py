from states.states import BaseStates
from loader import bot
from keyboards.category import create_categories_keyboard

@bot.message_handler(state=BaseStates.price)
def price_range_handler(m):
    with bot.retrieve_data(m.from_user.id, m.chat.id) as data:
        try:
            p1, p2 = m.text.split('-')
            data['from_price'] = int(p1)
            data['to_price'] = int(p2)
            bot.send_message(m.chat.id, f'Вы выбрали товары с ценами от {p1} до {p2},выберите категорию.',
                             reply_markup=create_categories_keyboard())
            bot.set_state(m.from_user.id, BaseStates.category, m.chat.id)
        except:
            bot.send_message(m.chat.id, 'Вы указали не правильно диапазон цен!')
            return