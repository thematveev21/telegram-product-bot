from states.states import BaseStates
from api.api import get_products_of_category
from loader import bot
from database.models import Query
from utils.helpers import create_text

@bot.message_handler(state=BaseStates.amount)
def amount_handler(m):
    if not m.text.isdigit():
        bot.send_message(m.chat.id, 'Вы ввели не число')
        return
    amount = int(m.text)
    with bot.retrieve_data(m.from_user.id, m.chat.id) as data:
        data['amount'] = amount
        products = get_products_of_category(data['category'])
        if data['type'] == '/low':
            products.sort(key=lambda x: x['price'])

        elif data['type'] == '/high':
            products.sort(key=lambda x: x['price'], reverse=True)

        elif data['type'] == '/custom':
            products = list(filter(
                lambda p: data['from_price'] <= p['price'] <= data['to_price'], products))

        products = products[0:amount]
        for p in products:
            bot.send_photo(m.chat.id, p['thumbnail'], create_text(p['title'], p['description'], p['price']))

        q = Query(
            chat_id=m.chat.id,
            category=data['category'],
            type=data['type'],
            amount=data['amount']
        )
        if data['type'] == '/custom':
            q.from_price = data['from_price']
            q.to_price = data['to_price']

        q.save()

    bot.delete_state(m.from_user.id, m.chat.id)