from loader import bot
from states.states import BaseStates
from keyboards.category import create_categories_keyboard

@bot.message_handler(commands=['low', 'high', 'custom'])
def basic_handler(m):
    bot.set_state(m.from_user.id, BaseStates.type, m.chat.id)
    with bot.retrieve_data(m.from_user.id, m.chat.id) as data:
        if m.text == '/low':
            bot.send_message(m.chat.id, 'Выборка товаров с низкой ценой.Выберите категорию',
                             reply_markup=create_categories_keyboard())
            data['type'] = '/low'
            bot.set_state(m.from_user.id, BaseStates.category, m.chat.id)

        elif m.text == '/high':
            bot.send_message(m.chat.id, 'Выборка товаров с высокой ценой.Выберите категорию',
                             reply_markup=create_categories_keyboard())
            data['type'] = '/high'
            bot.set_state(m.from_user.id, BaseStates.category, m.chat.id)

        elif m.text == '/custom':
            bot.send_message(m.chat.id, 'Вы выбрали кастомную филь-цию,укажите диапазон цен.')
            data['type'] = '/custom'
            bot.set_state(m.from_user.id, BaseStates.price, m.chat.id)