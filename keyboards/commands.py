from telebot.types import KeyboardButton, ReplyKeyboardMarkup
from config_data.config import DEFAULT_COMMANDS


def create_main_keyboard():
    keys = ReplyKeyboardMarkup(one_time_keyboard=True)
    for command, _ in DEFAULT_COMMANDS:
        b = KeyboardButton("/" + command)
        keys.add(b)

    return keys